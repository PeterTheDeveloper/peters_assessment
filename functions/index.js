const functions = require("firebase-functions");
const admin = require("firebase-admin");
const logger = functions.logger;
const { BigQuery } = require("@google-cloud/bigquery");
try {
  const credential = require("../config/ltg.json");
  process.env.FIRESTORE_EMULATOR_HOST = "localhost:8070";
  admin.initializeApp({
    credential: admin.credential.cert(credential),
    databaseURL: `https://${process.env.GCLOUD_PROJECT}.firebaseio.com`,
  });
} catch (err) {
  admin.initializeApp();
}

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const bigqueryClient = new BigQuery();

exports.getAnalytics = functions.https.onCall(async (data, context) => {
  const options = {
    keyFilename: "./local-travel-guide-analytics",
    projectId: "local-travel-guide",
    query: data.query,
    location: "US",
  };

  const [rows] = await bigqueryClient.query(options);

  return rows;
});

exports.saveKioskSignal = functions.https.onCall(async (data, context) => {
  const moment = require("moment-timezone");
  let timestamp = moment.tz("America/New_York").format();
  const db = admin.firestore();
  console.log(data.id);
  return db
    .collection("kiosk")
    .doc(data.id)
    .set({ last_signal: timestamp }, { merge: true })
    .then((msg) => {
      return true;
    })
    .catch((err) => {
      console.log(err);
      return err;
    });
});

exports.getInactiveKiosks = functions.https.onCall(async (data, context) => {
  const db = admin.firestore();
  const moment = require("moment-timezone");
  let inactive_kiosks = [];
  let kiosks = await db
    .collection("kiosk")
    .get()
    .then((snapshot) => {
      return snapshot.docs;
    });

  kiosks.forEach((kiosk) => {
    if (kiosk.data().last_signal) {
      let currentTime = moment.tz(Date.now(), "America/New_York");
      let last_signal = moment.tz(kiosk.data().last_signal, "America/New_York");
      let minutes = currentTime.diff(last_signal, "minutes", true);
      let duration;
      let days;
      let hours;
      if (minutes > 60 * 24) {
        days = minutes / (60 * 24);
        hours = 60 * (days % 1);
        duration = `${Math.floor(days)} days, ${Math.floor(hours)} hours`;
      } else if (minutes > 60) {
        hours = minutes / 60;
        minutes = 60 * (hours % 1);
        duration = `${Math.floor(hours)} hours, ${Math.floor(minutes)} minutes`;
      } else {
        duration = `${Math.floor(minutes)} minutes`;
      }
      if (minutes > 30) {
        inactive_kiosks.push({
          slug: kiosk.data().slug,
          signal: kiosk.data().last_signal,
          id: kiosk.id,
          name: kiosk.data().name,
          duration: duration,
        });
      }
    } else {
      if (kiosk.data().name) {
        inactive_kiosks.push({
          slug: kiosk.data().slug,
          signal: null,
          id: kiosk.id,
          name: kiosk.data().name,
          duration: "Has not sent a signal",
        });
      }
    }
  });

  if (!inactive_kiosks) inactive_kiosks = [];
  return db
    .collection("inactive_kiosk")
    .doc("all")
    .set({ kiosks: inactive_kiosks })
    .then(() => {
      return true;
    })
    .catch((err) => {
      console.log(err);
    });
  // loop through kiosks, find out what is not active. Write it to the inacetive_kiosk collection. Make sure admin is listening for changes.
});

exports.sendEmail = functions.https.onCall(async (data, context) => {
  let template_name = data.template_name;
  let from_email = data.from_email;
  let to_email = data.to_email;
  let global_merge_vars = data.global_merge_vars;
  let attachments = data.attachments ? data.attachments : [];
  let message = data.message;

  let mandrillData = {
    template_name: template_name,
    template_content: {},
    message: {
      merge_language: "handlebars",
      from_email: from_email,
      to: data.to_email,
      global_merge_vars: global_merge_vars,
      attachments: attachments,
    },
    merge: true,
  };

  let res = sendTemplate(mandrillData);
  return res;
});

function sendTemplate(opts) {
  const mandrillKey = functions.config().mandrill.key;
  var mandrill = require("mandrill-api/mandrill");
  console.log(mandrillKey);
  var mandrill_client = new mandrill.Mandrill(mandrillKey);
  return new Promise((resolve, reject) => {
    mandrill_client.messages.sendTemplate(opts, resolve, reject);
  });
}

exports.contractorOnDelete = functions
  .runWith({ memory: "2GB", timeoutSeconds: 540 })
  .firestore.document("contractor/{contractor_id}")
  .onDelete(async (snap, context) => {
    const db = admin.firestore();
    const batch = db.batch();

    return db.runTransaction(async (transaction) => {
      let contractors_in_range = await transaction.get(
        db
          .collectionGroup("contractors_in_range")
          .where("contractor_id", "==", context.params.contractor_id)
      );
      for (let i = 0; i < contractors_in_range.size; i++) {
        const doc = contractors_in_range.docs[i];
        transaction.delete(doc.ref);
      }
      transaction.delete(db.collection("contractor_locations").doc(snap.id));
    });
  });

exports.contractorOnUpdate = functions
  .runWith({ memory: "2GB", timeoutSeconds: 540 })
  .firestore.document("contractor/{contractor_id}")
  .onWrite(async (change, context) => {
    if (change.after.exists) {
      const db = admin.firestore();
      const batch = db.batch();
      const data = change.after.data();
      const range = data.accept_radius;
      const con_coords = data.address.coords;
      const truncated_contractor = {
        name: data.name,
        short_description: data.short_description,
        long_description: data.long_description,
        status: data.status,
        services: data.services,
        media: data.media,
        place_id: data.place_id,
        contractor_id: context.params.contractor_id,
        contact_info: data.contact_info,
        activity_types: data.activity_types.reduce(
          (acc, key) => ((acc[key] = true), acc),
          {}
        ),
        address: data.address,
      };

      if (data.hours) truncated_contractor.hours = data.hours;
      if (data.website) truncated_contractor.website = data.website;

      let in_range_locations = [];
      let previous_locations = await db
        .collection("contractor_locations")
        .doc(change.after.id)
        .get();
      if (previous_locations.exists) {
        previous_locations = previous_locations.data().locations_in_range;
        previous_locations = previous_locations.reduce(
          (acc, key) => ((acc[key] = true), acc),
          {}
        );
      } else {
        previous_locations = {};
      }

      let locations = await db.collection("location").get();
      locations.docs.forEach((d) => {
        let loc_coords = d.data().address.coords;
        if (
          calcDistance(
            con_coords.lat,
            con_coords.lng,
            loc_coords.lat,
            loc_coords.lng,
            "M"
          ) <= range
        ) {
          in_range_locations.push(d.id);
          batch.set(
            d.ref
              .collection("contractors_in_range")
              .doc(context.params.contractor_id),
            truncated_contractor,
            { merge: true }
          );
        } else if (previous_locations[d.id]) {
          batch.delete(
            d.ref
              .collection("contractors_in_range")
              .doc(context.params.contractor_id)
          );
        }
      });
      batch.set(db.collection("contractor_locations").doc(change.after.id), {
        locations_in_range: in_range_locations,
      });
      return batch.commit();
    } else {
      const db = admin.firestore();
      console.log("contractor_id", context.params.contractor_id);
      return db.runTransaction(async (transaction) => {
        let contractor_locations = await transaction.get(
          db
            .collection("contractor_locations")
            .doc(context.params.contractor_id)
        );
        contractor_locations = contractor_locations.data().locations_in_range;
        await transaction.delete(
          db
            .collection("contractor_locations")
            .doc(context.params.contractor_id)
        );
        return Promise.all(
          contractor_locations.map((l) =>
            transaction.delete(
              db.doc(
                `location/${l}/contractors_in_range/${context.params.contractor_id}`
              )
            )
          )
        );
      });
    }
  });

/**
 *
 * @param {Number} lat1 Latitude of point 1
 * @param {Number} lon1 Longitude of point 1
 * @param {Number} lat2 Latitude of point 2
 * @param {Number} lon2 Longitude of point 2
 * @param {"M"|"K"|"N"} unit Unit to find the distance in, Miles, Kilometers, or Nuatical Miles
 */

function calcDistance(lat1, lon1, lat2, lon2, unit = "M") {
  if (lat1 === lat2 && lon1 === lon2) {
    return 0;
  } else {
    var radlat1 = (Math.PI * lat1) / 180;
    var radlat2 = (Math.PI * lat2) / 180;
    var theta = lon1 - lon2;
    var radtheta = (Math.PI * theta) / 180;
    var dist =
      Math.sin(radlat1) * Math.sin(radlat2) +
      Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
      dist = 1;
    }
    dist = Math.acos(dist);
    dist = (dist * 180) / Math.PI;
    dist = dist * 60 * 1.1515;
    if (unit === "K") {
      dist = dist * 1.609344;
    }
    if (unit === "N") {
      dist = dist * 0.8684;
    }
    return dist;
  }
}

exports.sendText = functions.https.onCall((data, context) => {
  return sendText(data.name, data.number, data.url, data.custom_message);
});

async function sendText(name, number, url, custom_message = null, user) {
  const parsePhoneNumberFromString = require("libphonenumber-js")
    .parsePhoneNumberFromString;

  const twilioAccountSid = functions.config().twilio.sid;
  const twilioAuthToken = functions.config().twilio.auth_token;
  const twilioNumber = "+17813360048";
  const twilio_client = require("twilio")(twilioAccountSid, twilioAuthToken);
  const lookup_client = twilio_client.lookups.v1;

  const phoneNumber = parsePhoneNumberFromString(number, "US");
  if (phoneNumber) {
    number = phoneNumber.number;
  } else {
    console.error(err);
    throw new functions.https.HttpsError(
      "invalid-argument",
      "This was not a valid number!"
    );
  }

  try {
    let valid_number = lookup_client.phoneNumbers(number);
  } catch (err) {
    console.error(err);
    throw new functions.https.HttpsError(
      "invalid-argument",
      "This was not a valid number!"
    );
  }

  let body = custom_message;

  try {
    let result = await twilio_client.messages.create({
      body,
      from: twilioNumber,
      to: number,
    });
    if (result) {
      console.log(result);
      return true;
    } else {
      throw new functions.https.HttpsError(
        "unavailable",
        "Your message failed to send"
      );
    }
  } catch (err) {
    console.error(err);
    throw new functions.https.HttpsError(
      "unavailable",
      "Your message failed to send"
    );
  }
}
